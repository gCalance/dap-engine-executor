package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;


import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql.NoSqlConnectorRegistry;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql.DapNoSqlConstants.NOSQL_QUERY_STRING;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql.DapNoSqlConstants.NOSQL_TYPE_NAME;


public class NoSqlDataSetCreator  extends AbstarctDataSetCreater{


    @Autowired
    private NoSqlConnectorRegistry noSqlConnectorRegistry;

    @Override
    public Dataset<Row> getDataSet(Map<String, Object> properties) throws DapServerException {

        String noSqlType=String.valueOf(properties.get(NOSQL_TYPE_NAME));

        String query=String.valueOf(properties.get(NOSQL_QUERY_STRING));

        return noSqlConnectorRegistry.getNoSqlReader(noSqlType).executeQuery(getDapConnection(properties),query,properties);

    }
}
