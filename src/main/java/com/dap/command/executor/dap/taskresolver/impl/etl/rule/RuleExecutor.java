package com.dap.command.executor.dap.taskresolver.impl.etl.rule;

import com.dap.command.executor.dap.entity.Rule;
import com.dap.command.executor.dap.entity.RuleAction;
import com.dap.command.executor.dap.repository.RuleRepository;
import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.SqlDataSetCreator;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RuleExecutor extends AbstractTransformer {

    @Autowired
    SqlDataSetCreator sqlDataSetCreator;

    @Autowired
    private RuleRepository ruleRepository;

    @Override
    public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String, Object> properties){

        Integer processId=Integer.parseInt(String.valueOf(properties.get("processId")));
        Integer taskId=Integer.parseInt(String.valueOf(properties.get("taskId")));
        String workFlowUrl=String.valueOf(properties.get("workFlowUrl"));


        final  List<Rule> rules=ruleRepository.findByProcessIdAndTaskId(processId,taskId);


        Dataset<Row> dataset=sqlDataSetCreator.getExistingSqlDataSet(String.valueOf(properties.get("viewName")));

        for(int i=0;i<rules.size();i++){
            Dataset<Row> filterDataset=  dataset.filter(String.valueOf(rules.get(i).getExpression()));
            System.out.println("$$$$$$$$$$$$"+filterDataset.count());
            final Set<RuleAction> ras=rules.get(i).getRuleActions();
            filterDataset
                    .foreach((ForeachFunction<Row>) filterRow ->{
                        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+filterRow.get(filterRow.fieldIndex("processed")));
                        if(String.valueOf(filterRow.get(filterRow.fieldIndex("processed"))).equalsIgnoreCase("false")) {

                            Map<String, String> map = new HashMap<>();
                            map.put("id", "" + filterRow.get(filterRow.fieldIndex("id")));
                            ras.forEach(ra->{
                                RestTemplate restTemplate = new RestTemplate();
                                Map response = restTemplate.postForObject(String.format(workFlowUrl,ra.getActionUrl())
                                        , map, HashMap.class);
                                System.out.println("Response #########" + response);
                            });

                        }
                    });


        }
        return initialDataset;
    }

}
