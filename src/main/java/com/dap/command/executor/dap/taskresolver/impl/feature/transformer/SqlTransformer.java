package com.dap.command.executor.dap.taskresolver.impl.feature.transformer;

import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;

public class SqlTransformer extends AbstractTransformer {

    @Override
    public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String,Object> properties) {
        initialDataset.createOrReplaceTempView(String.valueOf(properties.get("name")));
        Dataset<Row> rescaledData = initialDataset.sqlContext().sql(String.valueOf(properties.get("sqlQuery")));
         rescaledData.createOrReplaceTempView(String.valueOf(properties.get("name")));
        return rescaledData;
           
    }

}
