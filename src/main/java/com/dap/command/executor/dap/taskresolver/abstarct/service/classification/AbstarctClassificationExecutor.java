package com.dap.command.executor.dap.taskresolver.abstarct.service.classification;

import java.io.IOException;
import java.util.Map;

import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.dap.command.executor.dap.entity.MLTaskInfo;
import com.dap.command.executor.dap.repository.MLTaskInfoRepository;

public abstract class AbstarctClassificationExecutor {

	@Value("${hdfs.url}")
	protected String HDFS_PATH;

	@Autowired
	MLTaskInfoRepository mlTaskInfoRepository;

	public double execute(Dataset<Row> initialDataset, Map<String, Object> properties) throws IOException {

		if (properties.get("features") != null) {
			initialDataset = applyVectorAssembler(initialDataset, properties);
		}

		if (properties.get("label") != null) {
			initialDataset = applyStringAssembler(initialDataset, properties);
		}

		// If Model
		if (properties.containsKey("isExportedTask")) {
			applyModel(initialDataset, properties);
			return 0.0;
		}

		return evaluate(initialDataset, properties);
	}

	/**
	 *
	 * @return
	 */
	protected Dataset<Row> applyModel(Dataset<Row> dataset, Map<String, Object> properties) {
		return null;
	};

	protected String generatePath(Map<String, Object> properties) {

		String modelId = String.valueOf(properties.get("modelId"));
		String taskName = String.valueOf(properties.get("name"));
		String modelName = String.valueOf(properties.get("modelName"));
		String path = modelId + "/" + taskName + "/" + modelName;
		return HDFS_PATH + "/" + path;

	}

	protected void writeIntoDb(Map<String, Object> properties) {
		String modelId = String.valueOf(properties.get("modelId"));
		String taskName = String.valueOf(properties.get("name"));
		String modelName = String.valueOf(properties.get("modelName"));
		String path = modelId + "/" + taskName + "/" + modelName;
		MLTaskInfo info = new MLTaskInfo();
		info.setModelPath(HDFS_PATH + "/" + path);
		info.setModelId(modelId);
		info.setModelName(modelName);
		info.setPrevTask(String.valueOf(properties.get("prevTask")));
		info.setAlgoName(String.valueOf(properties.get("taskType")));
		if (properties.containsKey("features")) {
			info.setFeatures(properties.get("features").toString());
		}
		mlTaskInfoRepository.save(info);
	}

	protected double evaluatePredictions(Dataset<Row> predictions) {
		// compute accuracy on the test set
		MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator().setLabelCol("label")
				.setPredictionCol("prediction").setMetricName("accuracy");
		return evaluator.evaluate(predictions);
	}

	protected void setOutputData(Dataset<Row> outuptData, Map<String, Object> properties) {
		outuptData.createOrReplaceTempView(String.valueOf(properties.get("name")));
	}

	private Dataset<Row> applyStringAssembler(Dataset<Row> initialDataset, Map<String, Object> properties) {
		return new StringIndexer().setInputCol(String.valueOf(properties.get("label"))).setOutputCol("label")
				.fit(initialDataset).transform(initialDataset);
	}

	private Dataset<Row> applyVectorAssembler(Dataset<Row> initialDataset, Map<String, Object> properties) {

		return new VectorAssembler().setInputCols(String.valueOf(properties.get("features")).split(","))
				.setOutputCol("features").transform(initialDataset);

	}

	protected abstract Double evaluate(Dataset<Row> initialDataset, Map<String, Object> properties) throws IOException;

}
