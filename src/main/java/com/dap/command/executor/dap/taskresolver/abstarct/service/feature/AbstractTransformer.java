package com.dap.command.executor.dap.taskresolver.abstarct.service.feature;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;

public abstract class AbstractTransformer {

    public abstract Dataset<Row> execute(Dataset<Row> initialDataset, Map<String,Object> properties);

}
