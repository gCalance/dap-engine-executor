package com.dap.command.executor.dap.taskresolver.impl.feature.extractors;

import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import org.apache.spark.ml.feature.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;

public class TfIdfExtractor extends AbstractTransformer {

    @Override
    public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String,Object> properties) {


        StringIndexer indexer = new StringIndexer()
                .setInputCol(String.valueOf(properties.get("label")))
                .setOutputCol("label");

        Dataset<Row> indexed = indexer.fit(initialDataset).transform(initialDataset);

        Tokenizer tokenizer = new Tokenizer()
                                .setInputCol(String.valueOf(properties.get("tokenizerInputColumn")))
                                .setOutputCol("words");
        Dataset<Row> wordsData = tokenizer.transform(indexed);

        int numFeatures = 20;
        HashingTF hashingTF = new HashingTF()
                .setInputCol("words")
                .setOutputCol("rawFeatures")
                .setNumFeatures(numFeatures);

        Dataset<Row> featurizedData = hashingTF.transform(wordsData);

        IDF idf = new IDF().setInputCol("rawFeatures").setOutputCol("features");
        IDFModel idfModel = idf.fit(featurizedData);

        Dataset<Row> rescaledData = idfModel.transform(featurizedData);
        rescaledData.createOrReplaceTempView(String.valueOf(properties.get("name")));

        return rescaledData;
    }


}
