package com.dap.command.executor.dap.config;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql.DapNoSqlConstants.NOSQL_CASSANDRA_TYPE_VALUE;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.read.stream.DapStreamConstants.STREAM_KAFKA_TYPE_VALUE;

import com.dap.command.executor.dap.controller.TaskListner;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.*;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql.CassandraReader;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql.NoSqlConnectorRegistry;
import com.dap.command.executor.dap.taskresolver.impl.classification.LinearSVMExecutor;
import com.dap.command.executor.dap.taskresolver.impl.classification.LogisticRegressionExecutor;
import com.dap.command.executor.dap.taskresolver.impl.classification.NaiveBayesExecutor;
import com.dap.command.executor.dap.taskresolver.impl.classification.RandomForestExecutor;
import com.dap.command.executor.dap.taskresolver.impl.etl.rule.RuleExecutor;
import com.dap.command.executor.dap.taskresolver.impl.executor.service.TaskExecutor;
import com.dap.command.executor.dap.taskresolver.impl.feature.extractors.TfIdfExtractor;
import com.dap.command.executor.dap.taskresolver.impl.feature.transformer.PythonScriptTransformer;
import com.dap.command.executor.dap.taskresolver.impl.feature.transformer.SqlTransformer;
import com.dap.command.executor.dap.taskresolver.impl.file.read.type.*;
import com.dap.command.executor.dap.taskresolver.impl.file.write.type.SqlDataWriter;
import com.dap.command.executor.dap.taskresolver.impl.read.stream.StreamReader;
import com.dap.command.executor.dap.taskresolver.impl.read.stream.StreamReaderRegistry;
import org.apache.spark.sql.SparkSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dap.command.executor.dap.taskresolver.abstarct.service.DapSparkSession;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.DataSetProvider;


@Configuration
public class DapTaskConfig {

	@Bean
	public SparkSession sparkSession() {

		return SparkSession.builder()
                //.master("local").appName("Dap Spark App")
				// .config("spark.some.config.option", "some-value")
				.getOrCreate();

	}


	@Bean
	public SqlDataSetCreator sqlDataSetCreator() {
		return new SqlDataSetCreator();
	}

	@Bean
	public FileDataSetCreator fileDataSetCreator() {
		return new FileDataSetCreator();
	}

	@Bean
	public CustomDataSetCreator customDataSetCreator() {
		return new CustomDataSetCreator();
	}

	@Bean
	public RestPullDataSetCreator restPullDataSetCreator() {
		return new RestPullDataSetCreator();
	}

	/*@Bean
	RuleService ruleService() {
		return new RuleService();
	}
*/
	@Bean
	public SqlTransformer sqlTransformer() {
		return new SqlTransformer();
	}

	@Bean
	public PythonScriptTransformer pythonScriptTransformer() {
		return new PythonScriptTransformer();
	}

	@Bean
	public TfIdfExtractor tfIdfExtractor() {
		return new TfIdfExtractor();
	}

	@Bean
	public NaiveBayesExecutor naiveBayesExecutor() {
		return new NaiveBayesExecutor();
	}

	@Bean
	public RandomForestExecutor randomForestExecutor() {
		return new RandomForestExecutor();
	}

	@Bean
	public LogisticRegressionExecutor logisticRegressionExecutor() {
		return new LogisticRegressionExecutor();
	}

	public LinearSVMExecutor linearSVMExecutor() {
		return new LinearSVMExecutor();
	}

	@Bean
	public RestReadReader restReadReader() {
		return new RestReadReader();
	}

	@Bean
	public SqlDataWriter sqlDataWriter() {
		return new SqlDataWriter();
	}

	@Bean
	public TaskExecutor taskExecutor() {
		return new TaskExecutor();
	}

	@Bean
	public DataSetProvider dataSetProvider() {
		return new DataSetProvider();
	}

	@Bean
	public CsvFileReader csvFileReader() {
		return new CsvFileReader();
	}

	@Bean
	public SqlReader sqlReader() {
		return new SqlReader();
	}

	@Bean
	public FileSystemReader fileSystemReader() {
		return new FileSystemReader();
	}

	@Bean
	public RuleExecutor ruleExecutor() {
		return new RuleExecutor();
	}

	@Bean
	public HdfsFileReader hdfsFileReader() {
		return new HdfsFileReader();
	}

	@Bean
	public FTPFileReader fTPFileReader() {
		return new FTPFileReader();
	}

	@Bean
	public S3FileReader s3FileReader() {
		return new S3FileReader();
	}

	@Bean
	public FileReaderRegistry fileReaderRegistry(CsvFileReader csvFileReader) {
		FileReaderRegistry fileReaderRegistry = new FileReaderRegistry();
		fileReaderRegistry.registerFileReader("csv", csvFileReader);
		return fileReaderRegistry;
	}

	@Bean
	public NoSqlDataSetCreator noSqlDataSetCreator() {
		return new NoSqlDataSetCreator();
	}

	@Bean
	public CassandraReader cassandraReader() {
		return new CassandraReader();
	}

	@Bean
	public NoSqlConnectorRegistry noSqlConnectorRegistry(CassandraReader cassandraReader) {
		NoSqlConnectorRegistry noSqlConnectorRegistry = new NoSqlConnectorRegistry();
		noSqlConnectorRegistry.registerNoSqlReader(NOSQL_CASSANDRA_TYPE_VALUE, cassandraReader);
		return noSqlConnectorRegistry;
	}

	@Bean
	public FileSystemReaderRegistry fileSystemReaderRegistry(HdfsFileReader hdfsFileReader, FTPFileReader fTPFileReader,
			S3FileReader s3FileReader) {
		FileSystemReaderRegistry fileSystemReaderRegistry = new FileSystemReaderRegistry();
		fileSystemReaderRegistry.registerFileReader("hdfs", hdfsFileReader);
		fileSystemReaderRegistry.registerFileReader("ftp", fTPFileReader);
		fileSystemReaderRegistry.registerFileReader("s3", s3FileReader);
		return fileSystemReaderRegistry;
	}

	@Bean
	public StreamDataSetCreator streamDataSetCreator() {
		return new StreamDataSetCreator();
	}

	@Bean
	public StreamReader streamReader() {
		return new StreamReader();
	}

	/*@Bean
	public KafkaStreamReader kafkaStreamReader() {
		return new KafkaStreamReader();
	}
*/
	@Bean
	public StreamReaderRegistry streamReaderRegistry() {
		StreamReaderRegistry streamReaderRegistry = new StreamReaderRegistry();
		//streamReaderRegistry.registerStreamReader(STREAM_KAFKA_TYPE_VALUE, kafkaStreamReader);
		return streamReaderRegistry;
	}

	@Bean
	TaskListner taskListner(){
		return new TaskListner();
	}

}
