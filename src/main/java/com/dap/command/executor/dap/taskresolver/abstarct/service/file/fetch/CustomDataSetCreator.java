package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;

import com.dap.command.executor.dap.exceptions.DapServerException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.Seq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomDataSetCreator extends AbstarctDataSetCreater {

    @Override
    public Dataset<Row> getDataSet(Map<String, Object> properties) throws DapServerException {

        Map<String,Object> data= (Map<String, Object>) properties.get("data");
        List<Row> rows=new ArrayList<>();
        String[] keys=new String[data.keySet().size()];
        List<StructField> fields=new ArrayList<>();
        keys=data.keySet().toArray(keys);
        String[] values=new String[data.keySet().size()];

        int index=0;
        for(String key:keys){
            values[index]=String.valueOf(data.get(key));
            fields.add(new StructField(key, DataTypes.StringType,true, Metadata.empty()));
            index++;
        }
        Row row= RowFactory.create(values);
        rows.add(row);

        StructType schema = new StructType(fields.toArray(new StructField[data.keySet().size()]));


        return getSparkSession().createDataFrame(rows,schema);
    }

    public  Dataset<Row> createDataFrame(List<Row> data, StructType schema){
        return getSparkSession().createDataFrame(data,schema);
    }


    public  Dataset<Row> csvSeqToDataSet(Seq<String> seqString){
        return getSparkSession().read().csv(seqString);
    }
}
