package com.dap.command.executor.dap;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ServerAsJms {


    private KafkaProducer kafkaProducer;
    private KafkaConsumer kafkaConsumer;

    private String consumerTopic;
    private String producerTopic;


    private void initConsumer() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("group.id", "test-group");
        kafkaConsumer=new KafkaConsumer(properties);
    }

    private void initProducer() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProducer=new KafkaProducer(properties);
    }

    public void init(String consumerTopic,String producerTopic){
        this.consumerTopic=consumerTopic;
        this.producerTopic=producerTopic;
        initConsumer();
        initProducer();
        List topics = new ArrayList();
        topics.add(consumerTopic);
        kafkaConsumer.subscribe(topics);
        try{
            while (true){
                ConsumerRecords records = kafkaConsumer.poll(10);
                records.iterator().forEachRemaining(c->{});
                for (Object record: records){
                    ConsumerRecord r= (ConsumerRecord) record;
                    System.out.println(String.format("Topic - %s, Partition - %d, Value: %s", r.topic(), r.partition(),
                            r.value()));
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }



    public void SendMessage(String message){

        try{

                kafkaProducer.send(new ProducerRecord(producerTopic, message, message ));

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void close(){
        kafkaProducer.close();
        kafkaConsumer.close();
    }



}
