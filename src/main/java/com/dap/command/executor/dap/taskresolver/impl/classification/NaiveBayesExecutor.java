package com.dap.command.executor.dap.taskresolver.impl.classification;

import com.dap.command.executor.dap.taskresolver.abstarct.service.classification.AbstarctClassificationExecutor;
import org.apache.spark.ml.classification.NaiveBayes;
import org.apache.spark.ml.classification.NaiveBayesModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.Map;

public class NaiveBayesExecutor extends AbstarctClassificationExecutor {

	@Value("${hdfs.url}")
	protected String HDFS_PATH;

	@Override
	public Double evaluate(Dataset<Row> initialDataset, Map<String, Object> properties) throws IOException {

		String[] splitRatio = String.valueOf(properties.get("splitRatio")).split(",");

		Dataset<Row>[] splits = initialDataset.randomSplit(
				new double[] { Double.parseDouble(splitRatio[0]), Double.parseDouble(splitRatio[1]) }, 1234L);
		Dataset<Row> train = splits[0];
		Dataset<Row> test = splits[1];

		// create the trainer and set its parameters
		NaiveBayes nb = new NaiveBayes();

		// train the model
		NaiveBayesModel model = nb.fit(train);

		// Select example rows to display.
		Dataset<Row> predictions = model.transform(test);
		if (properties.containsKey("isExport")) {
			// To DO move to abstarct layer

			model.write().overwrite().save(generatePath(properties));
			writeIntoDb(properties);

		}

		// create view for output predictions
		setOutputData(predictions, properties);

		// compute accuracy on the test set
		return evaluatePredictions(predictions);

	}

	@Override
	protected Dataset<Row> applyModel(Dataset<Row> dataset, Map<String, Object> properties) {
		Dataset<Row> newData = NaiveBayesModel.load(String.valueOf(properties.get("exportedModelPath")))
				.transform(dataset);
		newData.createOrReplaceTempView(String.valueOf(properties.get("name")));
		return newData;
	}

}
