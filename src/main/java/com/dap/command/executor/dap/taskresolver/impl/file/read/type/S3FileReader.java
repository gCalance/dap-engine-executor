package com.dap.command.executor.dap.taskresolver.impl.file.read.type;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.FILE_PATH;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.INPUT_TYPE;

import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.FileException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.BaseFileSystemReader;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.FileReader;

public class S3FileReader extends BaseFileSystemReader {

	@Override
	public Dataset<Row> getDataSet(SparkSession sparkSession, DapConnection dapConnection,
			Map<String, Object> properties) throws FileException {
		Configuration hadoopConfiguration = sparkSession.sparkContext().hadoopConfiguration();
		hadoopConfiguration.set("fs.s3a.access.key", dapConnection.getAwsAccessKey());
		hadoopConfiguration.set("fs.s3a.secret.key", dapConnection.getAwsSecretkey());
		hadoopConfiguration.set("fs.s3a.endpoint", dapConnection.getUrl());
		FileReader fileReader = fileReaderRegistry.getFileReader(String.valueOf(properties.get(INPUT_TYPE)));
		return fileReader.getDataSet(sparkSession.read(), preparePath(dapConnection, properties), properties);
	}

	private String preparePath(DapConnection dapConnection, Map<String, Object> properties) {
		String filePath = String.valueOf(properties.get(FILE_PATH)).startsWith("/")
				? String.valueOf(properties.get(FILE_PATH)).substring(1) : String.valueOf(properties.get(FILE_PATH));
		return "s3a://" + filePath;
	}

}
