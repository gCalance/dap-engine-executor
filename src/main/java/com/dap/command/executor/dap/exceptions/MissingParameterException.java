/**
 * 
 */
package com.dap.command.executor.dap.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author amar
 *
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MissingParameterException extends DapServerException {

	private static final long serialVersionUID = 1L;

	public MissingParameterException() {
		super();
	}

	public MissingParameterException(String message) {
		super(message);
	}

	public MissingParameterException(String message, Throwable cause) {
		super(message, cause);
	}

	public MissingParameterException(Throwable cause) {
		super(cause);
	}
}
