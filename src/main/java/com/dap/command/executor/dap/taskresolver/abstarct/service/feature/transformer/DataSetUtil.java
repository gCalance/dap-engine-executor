package com.dap.command.executor.dap.taskresolver.abstarct.service.feature.transformer;

import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.CustomDataSetCreator;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.python.core.PyDictionary;
import org.python.core.PyList;

import java.util.ArrayList;
import java.util.List;


public class DataSetUtil {


    private CustomDataSetCreator customDataSetCreator;

    public DataSetUtil(CustomDataSetCreator customDataSetCreator){
        this.customDataSetCreator = customDataSetCreator;
        }


    public Dataset<Row> createDataSet(PyList list){
        List<Row> rows=new ArrayList<>();
        StructType schema=null;
        for (int i = 0; i < list.size(); i++) {
            PyDictionary data= (PyDictionary) list.get(i);


            Object[] keys=new String[data.keySet().size()];
            List<StructField> fields=new ArrayList<>();
            keys=data.keySet().toArray(keys);
            for(Object key:keys){
                //Only support String values
                Row row=RowFactory.create(String.valueOf((data.get(key))));
                rows.add(row);
                fields.add(new StructField(key.toString(), DataTypes.StringType,true, Metadata.empty()));
            }


            schema = new StructType(fields.toArray(new StructField[data.keySet().size()]));

        }

        return schema==null?null:customDataSetCreator.createDataFrame(rows, schema);





    }
}
