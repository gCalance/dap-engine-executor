package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql;

public class DapNoSqlConstants {

    public static final String NOSQL_TYPE_NAME="nosqlType";

    public static final String NOSQL_QUERY_STRING="query";

    public static final String NOSQL_CASSANDRA_TYPE_VALUE="cassandra";

}
