package com.dap.command.executor.dap.taskresolver.impl.classification;

import java.io.IOException;
import java.util.Map;

import com.dap.command.executor.dap.taskresolver.abstarct.service.classification.AbstarctClassificationExecutor;
import org.apache.spark.ml.classification.LinearSVC;
import org.apache.spark.ml.classification.LinearSVCModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class LinearSVMExecutor extends AbstarctClassificationExecutor {

	@Override
	protected Double evaluate(Dataset<Row> initialDataset, Map<String, Object> properties) throws IOException {
		String[] splitRatio = String.valueOf(properties.get("splitRatio")).split(",");

		Dataset<Row>[] splits = initialDataset.randomSplit(
				new double[] { Double.parseDouble(splitRatio[0]), Double.parseDouble(splitRatio[1]) }, 1234L);
		Dataset<Row> train = splits[0];
		Dataset<Row> test = splits[1];

		// Set model parameters
		LinearSVC lsvc = new LinearSVC().setMaxIter(10).setRegParam(0.1);

		// Train the model
		LinearSVCModel model = lsvc.fit(train);
		Dataset<Row> predictions = model.transform(test);
		if (properties.containsKey("isExport")) {
			// To DO move to abstarct layer

			model.write().overwrite().save(generatePath(properties));
			writeIntoDb(properties);
		}

		// create view for output predictions
		setOutputData(predictions, properties);

		// compute accuracy on the test set
		return evaluatePredictions(predictions);
	}

	@Override
	protected Dataset<Row> applyModel(Dataset<Row> dataset, Map<String, Object> properties) {
		Dataset<Row> newData = LinearSVCModel.load(String.valueOf(properties.get("exportedModelPath")))
				.transform(dataset);
		newData.createOrReplaceTempView(String.valueOf(properties.get("name")));
		return newData;
	}

}
