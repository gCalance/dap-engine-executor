package com.dap.command.executor.dap.taskresolver.abstarct.service.file.write;

import com.dap.command.executor.dap.exceptions.FileException;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;

public interface FileWriter {

    public Dataset<Row> getDataSet(DataFrameReader dataFrameReader, Dataset<Row> data, Map<String, Object> properties) throws FileException;

}
