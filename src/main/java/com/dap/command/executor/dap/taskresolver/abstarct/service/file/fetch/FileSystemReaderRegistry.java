package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;

import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.BaseFileSystemReader;

import java.util.HashMap;
import java.util.Map;


public class FileSystemReaderRegistry {

	private Map<String, BaseFileSystemReader> registry = new HashMap<>();

	public BaseFileSystemReader getFileReader(String fileType) {
		return registry.get(fileType);
	}

	public void registerFileReader(String fileType, BaseFileSystemReader fileReader) {
		registry.put(fileType.toLowerCase(), fileReader);
	}

}
