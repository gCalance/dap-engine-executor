package com.dap.command.executor.dap.taskresolver.impl.file.read.type;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.BaseFileSystemReader;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.FILE_PATH;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.MULTILINE;;

public class FTPFileReader extends BaseFileSystemReader {

	@Override
	public Dataset<Row> getDataSet(SparkSession sparkSession, DapConnection dapConnection,
			Map<String, Object> properties) throws DapServerException {
		String ftpUrl = prepareFTPUrl(dapConnection, properties);
		List<String> dataFromFTP = readDataFromFTP(ftpUrl, Boolean.valueOf(String.valueOf(properties.get(MULTILINE))));
		return createSparkDataset(sparkSession, dataFromFTP,
				String.valueOf(properties.get(TaskPipelineConstants.INPUT_TYPE)));
	}

	private List<String> readDataFromFTP(String ftpUrl, Boolean multiline) throws DapServerException {
		List<String> rows = new ArrayList<>();
		BufferedReader in = null;
		if (multiline) {
			try {
				URL url = new URL(ftpUrl);
				URLConnection urlc = url.openConnection();
				InputStream is = urlc.getInputStream();
				in = new BufferedReader(new InputStreamReader(is));

				String line;
				while ((line = in.readLine()) != null) {
					rows.add(line);
				}

			} catch (IOException e) {
				throw new DapServerException(e);
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
						throw new DapServerException(e);
					}
				}
			}
		}
		return rows;
	}

	private Dataset<Row> createSparkDataset(SparkSession sparkSession, List<String> dataRows, String inputType) {
		Dataset<Row> dataset = null;
		Dataset<String> df = sparkSession.createDataset(dataRows, Encoders.STRING());
		switch (inputType.toLowerCase()) {
		case "csv":
			dataset = sparkSession.sqlContext().read().option("header", "true").csv(df);
			break;
		case "json":
			break;
		default:
			break;
		}
		return dataset;
	}

	private String prepareFTPUrl(DapConnection dapConnection, Map<String, Object> properties) {
		String url = dapConnection.getUrl().trim();
		StringBuilder ftpUrlBuilder = new StringBuilder("ftp://");
		if (null != dapConnection.getUserName() && null != dapConnection.getPassword()) {
			ftpUrlBuilder.append(dapConnection.getUserName()).append(":").append(dapConnection.getPassword())
					.append("@");
		}
		if (url.startsWith("ftp://")) {
			ftpUrlBuilder.append(url.substring(6));
		} else {
			ftpUrlBuilder.append(url);
		}
		if (null != dapConnection.getPort()) {
			ftpUrlBuilder.append(":").append(dapConnection.getPort());
		}
		String filePath = String.valueOf(properties.get(FILE_PATH)).startsWith("/")
				? String.valueOf(properties.get(FILE_PATH)) : "/" + String.valueOf(properties.get(FILE_PATH));
		ftpUrlBuilder.append(filePath);
		return ftpUrlBuilder.toString();
	}

}
