package com.dap.command.executor.dap.repository;

import com.dap.command.executor.dap.entity.Rule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RuleRepository extends CrudRepository<Rule,Integer> {

    Rule findOneByName(String name);

    Optional<Rule> findById(Integer id);

    Page<Rule> findByProcessIdAndTaskId(Integer processId, Integer taskId, Pageable pageable);

    List<Rule> findByProcessIdAndTaskId(Integer processId, Integer taskId);
}
