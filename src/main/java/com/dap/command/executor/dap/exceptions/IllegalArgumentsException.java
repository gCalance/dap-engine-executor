package com.dap.command.executor.dap.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalArgumentsException extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalArgumentsException() {
		super();
	}

	public IllegalArgumentsException(String message) {
		super(message);
	}

	public IllegalArgumentsException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalArgumentsException(Throwable cause) {
		super(cause);
	}

}
