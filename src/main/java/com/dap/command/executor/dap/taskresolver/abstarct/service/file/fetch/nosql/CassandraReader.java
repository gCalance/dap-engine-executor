package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.CustomDataSetCreator;
import com.datastax.driver.core.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CassandraReader implements NoSqlReader {

    @Autowired
    private CustomDataSetCreator customDataSetCreator;

    @Override
    public Dataset<org.apache.spark.sql.Row> executeQuery(DapConnection dapConnection, String query, Map<String, Object> properties) {

        CassandraConnector client=null;

        List<org.apache.spark.sql.Row> rows = new ArrayList<>();
        List<StructField> fields = new ArrayList<>();

        try {
            client = new CassandraConnector();
            client.connect(dapConnection.getUrl(), dapConnection.getPort());


            ResultSet result = client.getSession().execute(query);

            List<ColumnDefinitions.Definition> columnSequence = result.getColumnDefinitions().asList();


            for (ColumnDefinitions.Definition c : columnSequence) {
                // c.getType(); Use to determine type of columnn
                //Todo need provide proper type currently string is defaut
                fields.add(new StructField(c.getName(), DataTypes.StringType, true, org.apache.spark.sql.types.Metadata.empty()));
            }

            for (Row r : result) {
                int index = 0;
                String[] values = new String[columnSequence.size()];
                for (ColumnDefinitions.Definition c : columnSequence) {
                    values[index] = String.valueOf(r.getObject(c.getName()));
                    index++;
                }
                org.apache.spark.sql.Row row = RowFactory.create(values);
                rows.add(row);
            }

        }finally{
            if(client!=null){
                client.close();
            }
        }

        StructType schema = new StructType(fields.toArray(new StructField[fields.size()]));
        return customDataSetCreator.createDataFrame(rows,schema);



    }


    private static class CassandraConnector {

        private Cluster cluster;

        private Session session;


        public void connect(String node, Integer port) {
            Cluster.Builder b = Cluster.builder().addContactPoint(node);
            if (port != null) {
                b.withPort(port);
            }
            cluster = b.build();

            session = cluster.connect();
        }

        public Session getSession() {
            return session;
        }

        public Cluster getCluster() {
            return cluster;
        }

        public void close() {
            session.close();
            cluster.close();
        }
    }
}