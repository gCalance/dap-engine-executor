package com.dap.command.executor.dap.repository;

import com.dap.command.executor.dap.entity.MetaTaskInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MetaTaskInfoRepository extends CrudRepository<MetaTaskInfo, Integer> {

    List<MetaTaskInfo> findByHierarchyId(Integer id);

    MetaTaskInfo findByName(String name);

    boolean existsByHierarchyId(Integer id);
};
