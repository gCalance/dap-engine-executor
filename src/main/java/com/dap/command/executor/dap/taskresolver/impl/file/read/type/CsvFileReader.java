package com.dap.command.executor.dap.taskresolver.impl.file.read.type;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.FileException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.FileReader;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Map;

public class CsvFileReader implements FileReader {

	@Override
	public Dataset<Row> getDataSet(DataFrameReader dataFrameReader, String filePath, Map<String, Object> properties) {

		return dataFrameReader.option("header", String.valueOf(properties.get("header")))
				.option("multiline",
						properties.containsKey("multiline")
								? Boolean.valueOf(String.valueOf(properties.get("multiline"))) : false)
				.option("inferschema", "true").csv(filePath);
	}

	@Override
	public Dataset<Row> getDataSet(SparkSession sparkSession, DapConnection dapConnection,
			Map<String, Object> properties) throws FileException {
		// TODO Auto-generated method stub
		return null;
	}

}
