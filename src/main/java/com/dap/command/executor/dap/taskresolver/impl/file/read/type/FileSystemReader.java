package com.dap.command.executor.dap.taskresolver.impl.file.read.type;

import java.util.Map;

import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;


public class FileSystemReader extends AbstractTransformer {

	@Override
	public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String, Object> properties) {
		initialDataset.createOrReplaceTempView(String.valueOf(properties.get("name")));
		return initialDataset;
	}

}
