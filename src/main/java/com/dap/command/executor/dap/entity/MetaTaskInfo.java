package com.dap.command.executor.dap.entity;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class MetaTaskInfo {


    @Id
    @GeneratedValue
    private Integer id;

    @Size(min=2, message="Name should have atleast 2 characters")
    private String name;

    private String activitiModel;

    private String delegateExpression;


    private String className;



    private String executorClass;

    private Integer hierarchyId;

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    private TaskType taskType;

    public MetaTaskInfo(){
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivitiModel() {
        return activitiModel;
    }

    public void setActivitiModel(String activitiModel) {
        this.activitiModel = activitiModel;
    }

    public String getDelegateExpression() {
        return delegateExpression;
    }

    public void setDelegateExpression(String delegateExpression) {
        this.delegateExpression = delegateExpression;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getHierarchyId() {
        return hierarchyId;
    }


    public void setHierarchyId(Integer hierarchyId) {
        this.hierarchyId = hierarchyId;
    }

    public String getExecutorClass() {
        return executorClass;
    }

    public void setExecutorClass(String executorClass) {
        this.executorClass = executorClass;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    @Override
    public String toString() {
        return "MetaTaskInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", activitiModel='" + activitiModel + '\'' +
                ", delegateExpression='" + delegateExpression + '\'' +
                ", className='" + className + '\'' +
                ", executorClass='" + executorClass + '\'' +
                ", hierarchyId=" + hierarchyId +
                '}';
    }


}
