package com.dap.command.executor.dap.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class OperationNotSupportedException extends Exception {

	private static final long serialVersionUID = 1L;

	public OperationNotSupportedException() {
		super();
	}

	public OperationNotSupportedException(String message) {
		super(message);
	}

	public OperationNotSupportedException(String message, Throwable cause) {
		super(message, cause);
	}

	public OperationNotSupportedException(Throwable cause) {
		super(cause);
	}

}
