package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type;

import java.util.Map;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.FileReaderRegistry;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


public abstract class BaseFileSystemReader {

	@Value("${hdfs.url}")
	protected String HDFS_PATH;

	@Autowired
	protected FileReaderRegistry fileReaderRegistry;

	public abstract Dataset<Row> getDataSet(SparkSession sparkSession, DapConnection dapConnection,
			Map<String, Object> properties) throws DapServerException;

}
