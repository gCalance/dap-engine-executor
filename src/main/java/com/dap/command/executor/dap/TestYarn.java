package com.dap.command.executor.dap;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.Arrays;
import java.util.List;

public class TestYarn {

    public static void main(String[] args) throws InterruptedException {


        //ConfigurableApplicationContext context = SpringApplication.run(DapApplication.class, args);


        try {

            SparkConf conf = new SparkConf();
            //conf.setAppName("SqlTest").setMaster("yarn");
            //conf.set("spark.submit.deployMode", "client");
            // conf.set("HADOOP_CONF_DIR", "/home/gaurav/Downloads/hadooptesting/");
            // conf.set("spark.yarn.jars", "hdfs://172.18.0.5:9000/user/spark-libs.jar");
            // conf.set("spark.yarn.stagingDir", "hdfs://172.18.0.5:9000/user/root/");
            // conf.set("queue", "dap");
            // conf.set("spark.driver.host", "127.0.0.1");
            //conf.set("spark.driver.memory", "4g");

            SparkSession ss = SparkSession.builder().config(conf).getOrCreate();
            List<Row> data1 = Arrays.asList(
                    RowFactory.create(0.0, "Hi I heard about Spark"),
                    RowFactory.create(0.0, "I wish Java could use case classes"),
                    RowFactory.create(1.0, "Logistic regression models are neat")
            );
            StructType schema1 = new StructType(new StructField[]{
                    new StructField("label", DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField("sentence", DataTypes.StringType, false, Metadata.empty())
            });
            Dataset<Row> dataset = ss.sqlContext().createDataFrame(data1, schema1);
            dataset.createOrReplaceTempView("first");
            dataset.show();

            for(int i=0;i>10;i++){
                SparkSession newSession =ss.newSession();
                dataset = ss.sqlContext().createDataFrame(data1, schema1);
                dataset.createOrReplaceTempView("first"+i);
                newSession.sqlContext().sql("select * from first"+1).show();

                try{
                    newSession.sqlContext().sql("select * from first"+(i-1));
                }catch(Exception e){
                    System.out.println("@@@@@@@@@@@@@@Exception caught#############");
                }

            }
        /*ss.sqlContext().read().csv("/home/gaurav/work/calance/dap-engine/src/main/resources/testSql.csv")
                .createOrReplaceTempView("gaurav");
        ss.sqlContext().sql("select * from gaurav").show();
        System.out.println(ss.newSession().initialSessionOptions());
        SparkSession s1 = ss.newSession();
        s1.conf().set("a", "b");
        ss.conf();
        //ss.newSession();
        try {
            s1.sqlContext().sql("select * from gaurav").show();
        } catch (Exception e) {
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@ Exception @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        }
        ss.sqlContext().sql("select * from gaurav").show();*/

        }
        catch (Throwable e){
            e.printStackTrace();
        }
    }

}


