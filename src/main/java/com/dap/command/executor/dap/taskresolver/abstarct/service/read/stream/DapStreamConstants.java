package com.dap.command.executor.dap.taskresolver.abstarct.service.read.stream;

public class DapStreamConstants {

	public static final String STREAM_TYPE_NAME = "streamType";
	public static final String STREAM_KAFKA_TYPE_VALUE = "kafka";

}
