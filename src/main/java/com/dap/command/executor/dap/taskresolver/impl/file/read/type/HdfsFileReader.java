package com.dap.command.executor.dap.taskresolver.impl.file.read.type;


import java.util.Map;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.FileException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.BaseFileSystemReader;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.FileReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.FILE_PATH;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.INPUT_TYPE;


public class HdfsFileReader extends BaseFileSystemReader {

	// TOD0 -- Add kerberos authentication --
	@Override
	public Dataset<Row> getDataSet(SparkSession sparkSession, DapConnection dapConnection,
			Map<String, Object> properties) throws FileException {
		Configuration hadoopConfiguration = sparkSession.sparkContext().hadoopConfiguration();
		hadoopConfiguration.set("fs.defaultFS", createHdfsUrl(dapConnection));
		FileReader fileReader = fileReaderRegistry.getFileReader(String.valueOf(properties.get(INPUT_TYPE)));
		return fileReader.getDataSet(sparkSession.read(), preparePath(dapConnection, properties), properties);
	}

	private String preparePath(DapConnection dapConnection, Map<String, Object> properties) {
		String filePath = String.valueOf(properties.get(FILE_PATH));
		if (filePath.startsWith("/")) {
			return filePath;
		} else {
			return "/" + filePath;
		}
	}

	private String createHdfsUrl(DapConnection dapConnection) {
		String host = dapConnection.getUrl().startsWith("hdfs://") ? dapConnection.getUrl().substring(7)
				: dapConnection.getUrl();
		return "hdfs://" + host + ":" + dapConnection.getPort();
	}

}
