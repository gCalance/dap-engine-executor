package com.dap.command.executor.dap.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class DapConnection {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;
    private String type;
    private String userName;
    private String password;
    private String url;
    private Integer port;
	private String filePath;

	// properties for S3 connection
	private String awsAccessKey;
	private String awsSecretkey;

	// properties for HDFS connection
	private String keyTab;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getAwsAccessKey() {
		return awsAccessKey;
	}

	public void setAwsAccessKey(String awsAccessKey) {
		this.awsAccessKey = awsAccessKey;
	}

	public String getAwsSecretkey() {
		return awsSecretkey;
	}

	public void setAwsSecretkey(String awsSecretkey) {
		this.awsSecretkey = awsSecretkey;
	}

	public String getKeyTab() {
		return keyTab;
	}

	public void setKeyTab(String keyTab) {
		this.keyTab = keyTab;
	}

	@Override
	public String toString() {
		return "DapConnection{" +
				"id=" + id +
				", name='" + name + '\'' +
				", type='" + type + '\'' +
				", userName='" + userName + '\'' +
				", password='" + password + '\'' +
				", url='" + url + '\'' +
				", port=" + port +
				", filePath='" + filePath + '\'' +
				", awsAccessKey='" + awsAccessKey + '\'' +
				", awsSecretkey='" + awsSecretkey + '\'' +
				", keyTab='" + keyTab + '\'' +
				'}';
	}
}
