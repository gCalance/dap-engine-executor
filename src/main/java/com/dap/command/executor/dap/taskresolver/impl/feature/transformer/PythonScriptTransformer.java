package com.dap.command.executor.dap.taskresolver.impl.feature.transformer;

import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.transformer.DataSetUtil;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.CustomDataSetCreator;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import org.python.util.PythonInterpreter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class PythonScriptTransformer extends AbstractTransformer {


    @Autowired
    private CustomDataSetCreator customDataSetCreator;

    @Override
    public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String, Object> properties) {
        PythonInterpreter interp = new PythonInterpreter();
        interp.set("data_util",new DataSetUtil(customDataSetCreator));
        if(initialDataset!=null){

            interp.set("data",initialDataset);
        }


        String script=String.valueOf(properties.get("pyScript"));
       // String script="newdataset=[{'id': '5', 'name': 'ram','age':'5'}]\n" +
       //         "result = data_util.createDataSet(newdataset)\n" +
       //         "\n";
        interp.exec(script);
        Dataset<Row>  dataset=  interp.get("result",Dataset.class);

        dataset.createOrReplaceTempView(String.valueOf(properties.get("name")));
        return dataset;
    }
}
