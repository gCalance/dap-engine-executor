package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.CONNECTION_PROPERTY_NAME;

import java.util.Map;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.exceptions.MissingParameterException;
import com.dap.command.executor.dap.exceptions.NotFoundException;
import com.dap.command.executor.dap.repository.DapConnectionRepository;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


public abstract class AbstarctDataSetCreater {

    @Value("${hdfs.url}")
    protected String HDFS_PATH;



    @Autowired
    private SparkSession sparkSession;

    @Autowired
    private DapConnectionRepository dapConnectionRepository;

    public abstract Dataset<Row> getDataSet(Map<String,Object> properties) throws DapServerException;



   protected DapConnection getDapConnection(Map<String, Object> properties) throws DapServerException {
        if(!properties.containsKey(CONNECTION_PROPERTY_NAME)) throw new MissingParameterException("Connection Id not Found in task Properties");

        int connectionId= Integer.parseInt(String.valueOf(properties.get(CONNECTION_PROPERTY_NAME)));
        return dapConnectionRepository.findById(connectionId).orElseThrow(NotFoundException::new);
    }


    public SparkSession getSparkSession() {
        return sparkSession;
    }
}
