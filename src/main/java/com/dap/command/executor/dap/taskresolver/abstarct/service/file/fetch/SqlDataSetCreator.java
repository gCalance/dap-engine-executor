package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;


import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.DapServerException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;

public class SqlDataSetCreator extends AbstarctDataSetCreater{

    @Override
    public Dataset<Row> getDataSet(Map<String, Object> properties) throws DapServerException {
        String sqlQuery= String.valueOf(properties.get("sqlQuery"));

        DapConnection dapConnection = getDapConnection(properties);


        //destination database table
        return  getSparkSession().read().format("jdbc").option("url", dapConnection.getUrl())
                .option("driver", String.valueOf(properties.get("driverClass")))
                .option("dbtable", sqlQuery)
                .option("user", dapConnection.getUserName())
                .option("password", dapConnection.getPassword())
                .load();
    }


    public Dataset<Row> getExistingSqlDataSet(String viewName){
        return getSparkSession().sqlContext().table(viewName);
    }


}
