package com.dap.command.executor.dap.taskresolver.abstarct.service.read.stream;

import java.util.Map;

import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.CustomDataSetCreator;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class AbstractStreamReader {

	@Autowired
	protected CustomDataSetCreator customDataSetCreator;

	public abstract Dataset<Row> getDataSet(SparkSession sparkSession, DapConnection dapConnection,
			Map<String, Object> properties) throws DapServerException;

}
