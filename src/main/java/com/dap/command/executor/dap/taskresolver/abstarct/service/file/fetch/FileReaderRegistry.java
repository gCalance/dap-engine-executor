package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;


import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.FileReader;

import java.util.HashMap;
import java.util.Map;

public class FileReaderRegistry {

    private Map<String, FileReader> registry=new HashMap<>();


    public FileReader getFileReader(String fileType){
        return registry.get(fileType.toLowerCase());
    }

    public void registerFileReader(String fileType,FileReader fileReader){
        registry.put(fileType.toLowerCase(),fileReader);
    }

}
