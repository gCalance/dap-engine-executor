package com.dap.command.executor.dap;

import com.dap.command.executor.dap.dto.Greeting;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class DapApplication {

	public static void main(String[] args) throws InterruptedException {

        System.out.println("###############"+args.length);
        MachineArgs.setArgs(args);

        ConfigurableApplicationContext context = SpringApplication.run(DapApplication.class, args);

       /* SparkSession s=context.getBeanFactory().getBean("sparkSession",SparkSession.class);

        testLocal(s);



        //client.close();

        context.close();*/


    }

    private static void testLocal(SparkSession ss) {
        try {

           // SparkConf conf = new SparkConf();
          //  conf.setAppName("SqlTest").setMaster("yarn");
          //  conf.set("spark.submit.deployMode", "cluster");
            // conf.set("HADOOP_CONF_DIR", "/home/gaurav/Downloads/hadooptesting/");
           //  conf.set("spark.yarn.jars", "hdfs://testbed-master:9000/user/spark-libs.jar");
           //  conf.set("spark.yarn.stagingDir", "hdfs://testbed-master:9000/user/root/");
            // conf.set("queue", "dap");
           //  conf.set("spark.driver.host", "127.0.0.1");
            //conf.set("spark.driver.memory", "4g");
          //  SparkSession ss = SparkSession.builder().config(conf).getOrCreate();
            List<Row> data1 = Arrays.asList(
                    RowFactory.create(0.0, "Hi I heard about Spark"),
                    RowFactory.create(0.0, "I wish Java could use case classes"),
                    RowFactory.create(1.0, "Logistic regression models are neat")
            );
            StructType schema1 = new StructType(new StructField[]{
                    new StructField("label", DataTypes.DoubleType, false, Metadata.empty()),
                    new StructField("sentence", DataTypes.StringType, false, Metadata.empty())
            });
            Dataset<Row> dataset = ss.sqlContext().createDataFrame(data1, schema1);
            dataset.createOrReplaceTempView("first");
            dataset.show();

            for(int i=0;i<10;i++){
                SparkSession newSession =ss.newSession();
                dataset = newSession.sqlContext().createDataFrame(data1, schema1);
                dataset.createOrReplaceTempView("first"+i);
                newSession.sqlContext().sql("select * from first"+i).show();

                try{
                    newSession.sqlContext().sql("select * from first"+(i-1));
                }catch(Exception e){
                    System.out.println("@@@@@@@@@@@@@@Exception caught#############");
                }

            }


        }
        catch (Throwable e){
            e.printStackTrace();
        }
    }

}

