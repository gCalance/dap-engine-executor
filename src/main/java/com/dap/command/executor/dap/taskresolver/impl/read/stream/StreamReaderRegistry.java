package com.dap.command.executor.dap.taskresolver.impl.read.stream;

import com.dap.command.executor.dap.taskresolver.abstarct.service.read.stream.AbstractStreamReader;

import java.util.HashMap;
import java.util.Map;


public class StreamReaderRegistry {

	private Map<String, AbstractStreamReader> registry = new HashMap<>();

	public AbstractStreamReader getStreamReader(String streamType) {
		return registry.get(streamType);
	}

	public void registerStreamReader(String streamType, AbstractStreamReader streamReader) {
		registry.put(streamType, streamReader);
	}

}
