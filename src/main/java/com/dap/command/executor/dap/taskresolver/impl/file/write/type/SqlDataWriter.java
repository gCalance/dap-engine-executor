package com.dap.command.executor.dap.taskresolver.impl.file.write.type;

import com.dap.command.executor.dap.exceptions.FileException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.write.FileWriter;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;
import java.util.Properties;

public class SqlDataWriter extends AbstractTransformer {


    @Override
    public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String, Object> properties) {
        Properties prop= new Properties();

        prop.setProperty("driver", String.valueOf(properties.get("driverClass")));
        prop.setProperty("user", String.valueOf(properties.get("username")));
        prop.setProperty("password", String.valueOf(properties.get("password")));

        //jdbc mysql url - destination database is named "data"
        String url = String.valueOf(properties.get("url"));

        //destination database table
        String table = String.valueOf(properties.get("tableName"));

        //write data from spark dataframe to database
        initialDataset.write().mode("append").jdbc(url, table, prop);

        return initialDataset;
    }
}
