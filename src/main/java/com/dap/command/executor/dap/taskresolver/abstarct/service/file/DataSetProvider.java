package com.dap.command.executor.dap.taskresolver.abstarct.service.file;


import java.util.Map;

import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import org.springframework.beans.factory.annotation.Autowired;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.*;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql.DapNoSqlConstants.NOSQL_TYPE_NAME;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.read.stream.DapStreamConstants.STREAM_TYPE_NAME;


public class DataSetProvider {


	@Autowired
	private SqlDataSetCreator sqlDataSetCreator;

	@Autowired
	private FileDataSetCreator fileDataSetCreator;

	@Autowired
	private CustomDataSetCreator customDataSetCreator;

	@Autowired
	private NoSqlDataSetCreator noSqlDataSetCreator;

	@Autowired
	private StreamDataSetCreator streamDataSetCreator;

	public Dataset<Row> getDataSet(Map<String, Object> properties) throws DapServerException {

		if (properties.containsKey(DEPENDENCY)) {
			return sqlDataSetCreator.getExistingSqlDataSet(String.valueOf(properties.get(DEPENDENCY)));
		}

		if (properties.get(INPUT_TYPE) != null && properties.get(INPUT_PATH) != null) {
			return fileDataSetCreator.getDataSet(properties);
		}

		if (properties.get(INPUT_TYPE) != null
				&& String.valueOf(properties.get(INPUT_TYPE)).equalsIgnoreCase(HTTP_PROPERTY_NAME)) {
			return sqlDataSetCreator.getDataSet(properties);
		}

		if (properties.get(FILE_SYSTEM_NAME) != null) {
			return fileDataSetCreator.getDataSet(properties);
		}

		if (properties.get(INPUT_TYPE) != null && properties.get(INPUT_PATH) != null) {
			return fileDataSetCreator.getDataSet(properties);
		}

		// Need to remove when user specifc task url done
		if (properties.containsKey("data")) {
			return customDataSetCreator.getDataSet(properties);
		}

		if (properties.get(INPUT_TYPE) != null
				&& String.valueOf(properties.get(INPUT_TYPE)).equalsIgnoreCase(JDBC_PROPERTY_NAME)) {
			return sqlDataSetCreator.getDataSet(properties);
		}

		if (properties.get(INPUT_TYPE) != null
				&& String.valueOf(properties.get(INPUT_TYPE)).equalsIgnoreCase(NOSQL_TYPE_NAME)) {
			return noSqlDataSetCreator.getDataSet(properties);
		}

		if (properties.get(STREAM_TYPE_NAME) != null) {
			return streamDataSetCreator.getDataSet(properties);
		}

		return null;
	}

}
