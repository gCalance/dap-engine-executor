package com.dap.command.executor.dap.taskresolver.abstarct.service.constants;

public class TaskPipelineConstants {

	public static final String NAME = "name";

	public static final String INPUT_TYPE = "inputType";

	public static final String INPUT_PATH = "inputPath";

	public static final String FILE_PATH = "filePath";

	public static final String DEPENDENCY = "dependency";

	public static final String HTTP_PROPERTY_NAME = "httpsource";

	public static final String JDBC_PROPERTY_NAME = "jdbc";

	public static final String CONNECTION_PROPERTY_NAME = "connectionId";

	public static final String FILE_SYSTEM_NAME = "fileSystem";

	public static final String MULTILINE = "multiline";

	public static final String HEADER = "header";

	public static final String SCHEMA = "schema";

	public static final String KAFKA_TOPIC_NAME = "topic";

	public static final String PROCESS_ID = "processId";

	public static final String STREAM_TERMINATION_MESSAGE = "terminationMessage";

	public static final String STREAM_POLL_DURATION = "pollDuration";

	public static final String STREAM_APPEND = "append";

}
