package com.dap.command.executor.dap.entity;

import javax.persistence.*;

@Entity
public class MLTaskInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String modelName;
	private String algoName;
	private String modelId;
	private String deploymentId;
	private String modelPath;
	@Lob
	private String features;
	private int numFeatures;
	private String prevTask;

	private long createdAt;

	private long modifiedAt;

	public MLTaskInfo() {
		super();
	}

	public MLTaskInfo(String modelName, String algoName, String modelId, String deploymentId, String modelPath,
			String features, int numFeatures, String prevTask, long createdAt, long modifiedAt) {
		super();
		this.modelName = modelName;
		this.algoName = algoName;
		this.modelId = modelId;
		this.deploymentId = deploymentId;
		this.modelPath = modelPath;
		this.features = features;
		this.numFeatures = numFeatures;
		this.prevTask = prevTask;
		this.createdAt = createdAt;
		this.modifiedAt = modifiedAt;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the modelId
	 */
	public String getModelId() {
		return modelId;
	}

	/**
	 * @param modelId
	 *            the modelId to set
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	/**
	 * @return the deploymentId
	 */
	public String getDeploymentId() {
		return deploymentId;
	}

	/**
	 * @param deploymentId
	 *            the deploymentId to set
	 */
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	/**
	 * @return the modelName
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * @param modelName
	 *            the modelName to set
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/**
	 * @return the algoName
	 */
	public String getAlgoName() {
		return algoName;
	}

	/**
	 * @param algoName
	 *            the algoName to set
	 */
	public void setAlgoName(String algoName) {
		this.algoName = algoName;
	}

	/**
	 * @return the modelPath
	 */
	public String getModelPath() {
		return modelPath;
	}

	/**
	 * @param modelPath
	 *            the modelPath to set
	 */
	public void setModelPath(String modelPath) {
		this.modelPath = modelPath;
	}

	/**
	 * @return the features
	 */
	public String getFeatures() {
		return features;
	}

	/**
	 * @param features
	 *            the features to set
	 */
	public void setFeatures(String features) {
		this.features = features;
	}

	/**
	 * @return the createdAt
	 */
	public long getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the modifiedAt
	 */
	public long getModifiedAt() {
		return modifiedAt;
	}

	/**
	 * @param modifiedAt
	 *            the modifiedAt to set
	 */
	public void setModifiedAt(long modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	/**
	 * @return the numFeatures
	 */
	public int getNumFeatures() {
		return numFeatures;
	}

	/**
	 * @param numFeatures
	 *            the numFeatures to set
	 */
	public void setNumFeatures(int numFeatures) {
		this.numFeatures = numFeatures;
	}

	/**
	 * @return the prevTask
	 */
	public String getPrevTask() {
		return prevTask;
	}

	/**
	 * @param prevTask
	 *            the prevTask to set
	 */
	public void setPrevTask(String prevTask) {
		this.prevTask = prevTask;
	}

}
