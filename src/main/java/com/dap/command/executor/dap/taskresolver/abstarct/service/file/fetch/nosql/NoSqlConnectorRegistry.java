package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql;


import java.util.HashMap;
import java.util.Map;

public class NoSqlConnectorRegistry {

    private Map<String, NoSqlReader> registry=new HashMap<>();


    public NoSqlReader getNoSqlReader(String name){
        return registry.get(name.toLowerCase());
    }

    public void registerNoSqlReader(String name,NoSqlReader noSqlReader){
        registry.put(name.toLowerCase(),noSqlReader);
    }

}

