package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;


import java.util.Map;

import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.exceptions.FileException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.BaseFileSystemReader;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type.FileReader;
import com.dap.command.executor.dap.taskresolver.abstarct.service.util.StringUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.FILE_SYSTEM_NAME;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.INPUT_PATH;
import static com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants.INPUT_TYPE;


public class FileDataSetCreator extends AbstarctDataSetCreater {

	@Autowired
	private FileReaderRegistry fileReaderRegistry;

	@Autowired
	private FileSystemReaderRegistry fileSystemReaderRegistry;

	@Override
	public Dataset<Row> getDataSet(Map<String, Object> properties) throws DapServerException {

		String inputPath = String.valueOf(properties.get(INPUT_PATH));
		String inputType = String.valueOf(properties.get(INPUT_TYPE));
		String fileSystemName = String.valueOf(properties.get(FILE_SYSTEM_NAME));
		return getDatasetByFile(fileSystemName, inputType, inputPath, properties);

	}

	protected Dataset<Row> getDatasetByFile(String fileSystemName, String inputType, String inputPath,
			Map<String, Object> properties) throws DapServerException {
		BaseFileSystemReader fileSystemReader = fileSystemReaderRegistry.getFileReader(fileSystemName);
		if (fileSystemReader == null)
			throw new FileException("No FileReader Found for filesystem " + fileSystemName);
		return fileSystemReader.getDataSet(getSparkSession(), getDapConnection(properties), properties);
	}

	protected Dataset<Row> getDatasetByFile(String inputType, String inputPath, Map<String, Object> properties)
			throws FileException {
		FileReader fileReader = fileReaderRegistry.getFileReader(inputType);

		if (fileReader == null)
			throw new FileException("No FileReader Found for type " + inputType);

		return fileReaderRegistry.getFileReader(inputType).getDataSet(getSparkSession().read(),
				preparePath(inputPath, inputType), properties);
	}

	private String preparePath(String inputPath, String type) throws FileException {

		if (StringUtil.isStringNUllOrEmpty(inputPath))
			throw new FileException("Input Path is empty or null");

		return getFilePath(getFileSystem(inputPath), inputPath);

	}

	private String getFilePath(FileSystemTypes type, String inputPath) throws FileException {
		if (null == type)
			throw new FileException("FileSytem Type is null");

		if (type.equals(FileSystemTypes.SERVER_DIRECTORY))
			return inputPath.contains("file:") ? inputPath : "file://" + inputPath;

		return HDFS_PATH + inputPath;

	}

	private FileSystemTypes getFileSystem(String inputPath) {

		if (inputPath.contains("file:"))
			return FileSystemTypes.SERVER_DIRECTORY;

		return FileSystemTypes.HDFS;

	}

}
