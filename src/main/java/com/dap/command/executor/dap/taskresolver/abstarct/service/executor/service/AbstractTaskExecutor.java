package com.dap.command.executor.dap.taskresolver.abstarct.service.executor.service;


import com.dap.command.executor.dap.entity.MetaTaskInfo;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.classification.AbstarctClassificationExecutor;
import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.DataSetProvider;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Map;

public abstract class AbstractTaskExecutor implements BeanFactoryAware {

    private BeanFactory beanFactory;

    @Autowired
    DataSetProvider dataSetProvider;

    protected Dataset<Row> getDataSet(MetaTaskInfo taskInfo, Map<String, Object> properties) throws DapServerException {

        return dataSetProvider.getDataSet(properties);

    }

    public abstract Map<String,Object>  execute(MetaTaskInfo taskInfo,Map<String,Object> properties) throws DapServerException, IOException;

    protected Dataset<Row> executeEtl(String etlTransformer,Dataset<Row> dataset,Map<String,Object>params){
        return beanFactory.getBean(etlTransformer, AbstractTransformer.class).execute(dataset,params);
    }

    protected Double executeClassification(String etlTransformer,Dataset<Row> dataset,Map<String,Object>params) throws IOException {
        return beanFactory.getBean(etlTransformer, AbstarctClassificationExecutor.class).execute(dataset,params);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
            this.beanFactory=beanFactory;
    }

}
