package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;

import java.util.Map;

import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.impl.read.stream.StreamReaderRegistry;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;

import static com.dap.command.executor.dap.taskresolver.abstarct.service.read.stream.DapStreamConstants.STREAM_TYPE_NAME;


public class StreamDataSetCreator extends AbstarctDataSetCreater {

	@Autowired
	private StreamReaderRegistry streamReaderRegistry;

	@Override
	public Dataset<Row> getDataSet(Map<String, Object> properties) throws DapServerException {
		String streamType = String.valueOf(properties.get(STREAM_TYPE_NAME));
		return streamReaderRegistry.getStreamReader(streamType).getDataSet(getSparkSession(),
				getDapConnection(properties), properties);
	}

}
