package com.dap.command.executor.dap.repository;

import com.dap.command.executor.dap.entity.DapConnection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface DapConnectionRepository extends JpaRepository<DapConnection, Integer> {



}
