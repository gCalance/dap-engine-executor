package com.dap.command.executor.dap.taskresolver.abstarct.service;

import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.SparkSessionExtensions;
import org.apache.spark.sql.internal.SessionState;
import org.apache.spark.sql.internal.SharedState;
import scala.Option;

public class DapSparkSession extends SparkSession {


    public DapSparkSession(SparkContext sparkContext, Option<SharedState> existingSharedState, Option<SessionState> parentSessionState, SparkSessionExtensions extensions) {
        super(sparkContext, existingSharedState, parentSessionState, extensions);
    }

    public DapSparkSession(SparkContext sc) {
        super(sc);
    }
}
