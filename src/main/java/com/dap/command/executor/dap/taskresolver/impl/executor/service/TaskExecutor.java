package com.dap.command.executor.dap.taskresolver.impl.executor.service;

import com.dap.command.executor.dap.entity.MetaTaskInfo;
import com.dap.command.executor.dap.entity.TaskType;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.taskresolver.abstarct.service.executor.service.AbstractTaskExecutor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TaskExecutor extends AbstractTaskExecutor {


    @Override
    public Map<String,Object>  execute(MetaTaskInfo taskInfo, Map<String, Object> properties) throws DapServerException, IOException {

        Map<String,Object> response=new HashMap<>();

        System.out.println("I am in Task Executor");

        Dataset<Row> initialDataSetinitialDataSet= getDataSet(taskInfo,properties);

        if(TaskType.ETL.equals(taskInfo.getTaskType())){
            response.put("message",
                    executeEtl(taskInfo.getExecutorClass(),initialDataSetinitialDataSet,properties));
        }

        else if(TaskType.ML.equals(taskInfo.getTaskType())){
            response.put("message",executeClassification(taskInfo.getExecutorClass(),initialDataSetinitialDataSet,properties));

        }

        response.put("success","true");

        return response;

    }




}
