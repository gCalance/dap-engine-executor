package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.type;


import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.FileException;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Map;

public interface FileReader {

	public Dataset<Row> getDataSet(DataFrameReader dataFrameReader, String filePath, Map<String, Object> properties)
			throws FileException;

	public Dataset<Row> getDataSet(SparkSession sparkSession, DapConnection dapConnection,
                                   Map<String, Object> properties) throws FileException;
}
