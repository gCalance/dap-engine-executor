package com.dap.command.executor.dap.taskresolver.impl.read.stream;

import java.util.Map;

import com.dap.command.executor.dap.taskresolver.abstarct.service.constants.TaskPipelineConstants;
import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;


public class StreamReader extends AbstractTransformer {

	@Override
	public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String, Object> properties) {
		SparkSession sparkSession = initialDataset.sparkSession();
		boolean append = Boolean.parseBoolean(String.valueOf(properties.get(TaskPipelineConstants.STREAM_APPEND)));
		if (append && sparkSession.catalog().tableExists(String.valueOf(properties.get(TaskPipelineConstants.NAME)))) {
			return sparkSession.table(String.valueOf(properties.get(TaskPipelineConstants.NAME))).join(initialDataset);
		} else {
			initialDataset.createOrReplaceTempView(String.valueOf(properties.get(TaskPipelineConstants.NAME)));
		}
		return initialDataset;
	}

}
