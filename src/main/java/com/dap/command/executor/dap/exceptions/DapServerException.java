package com.dap.command.executor.dap.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class DapServerException extends Exception {



        private static final long serialVersionUID = 1L;

        public DapServerException() {
            super();

        }

    public DapServerException (String message) {
        super(message);
    }

    public DapServerException(String message, Throwable cause) {
            super(message,cause);
    }

    public DapServerException(Throwable cause) {
            super(cause);

    }
}
