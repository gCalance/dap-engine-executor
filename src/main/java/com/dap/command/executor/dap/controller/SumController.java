/*
package com.dap.command.executor.dap.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.calance.dap.config.Model;
import com.dap.command.executor.dap.entity.MetaTaskInfo;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.repository.MetaTaskInfoRepository;
import com.dap.command.executor.dap.taskresolver.impl.executor.service.TaskExecutor;
import org.apache.commons.collections.map.HashedMap;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SumController {
	
	//@Autowired
	ReplyingKafkaTemplate<String, Model,Model> kafkaTemplate;

	//@Autowired
	MetaTaskInfoRepository metaTaskInfoRepository;
	
	@Value("${kafka.topic.request-topic}")
	String requestTopic;
	
	@Value("${kafka.topic.requestreply-topic}")
	String requestReplyTopic;

	//@Autowired
	TaskExecutor taskExecutor;
	
	@ResponseBody
	@PostMapping(value="/sum",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Model sum(@RequestBody Model request) throws InterruptedException, ExecutionException {
		// create producer record
		ProducerRecord<String, Model> record = new ProducerRecord<String, Model>(requestTopic, request);
		// set reply topic in header
		record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, requestReplyTopic.getBytes()));
		// post in kafka topic
		RequestReplyFuture<String, Model, Model> sendAndReceive = kafkaTemplate.sendAndReceive(record);

		// confirm if producer produced successfully
		SendResult<String, Model> sendResult = sendAndReceive.getSendFuture().get();
		
		//print all headers
		sendResult.getProducerRecord().headers().forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));
		
		// get consumer record
		ConsumerRecord<String, Model> consumerRecord = sendAndReceive.get();
		// return consumer value
		return consumerRecord.value();		
	}


	@ResponseBody
	@PostMapping(value="/execute",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String,Object>> getData(@RequestBody Map<String,Object> request) throws IOException, DapServerException {


		MetaTaskInfo m = metaTaskInfoRepository.findByName(String.valueOf(request.get("taskType")));
			return new ResponseEntity(taskExecutor.execute(m,request), HttpStatus.OK);
	}

}*/
