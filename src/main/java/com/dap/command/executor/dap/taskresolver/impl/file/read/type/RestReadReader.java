package com.dap.command.executor.dap.taskresolver.impl.file.read.type;


import com.dap.command.executor.dap.taskresolver.abstarct.service.feature.AbstractTransformer;
import com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.SqlDataSetCreator;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class RestReadReader extends AbstractTransformer {

    @Autowired
    SqlDataSetCreator sqlDataSetCreator;


    @Override
    public Dataset<Row> execute(Dataset<Row> initialDataset, Map<String, Object> properties){
       String tableName= String.valueOf(properties.get("tableName"));
          initialDataset.createOrReplaceTempView(tableName);
        return sqlDataSetCreator.getExistingSqlDataSet(tableName);
    }
}
