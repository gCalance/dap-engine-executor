package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch.nosql;

import com.dap.command.executor.dap.entity.DapConnection;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;

public interface NoSqlReader {

    public Dataset<Row> executeQuery(DapConnection dapConnection, String query, Map<String, Object> properties);

}
