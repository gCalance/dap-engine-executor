package com.dap.command.executor.dap.taskresolver.abstarct.service.file.fetch;


import com.dap.command.executor.dap.entity.DapConnection;
import com.dap.command.executor.dap.exceptions.DapServerException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Map;

public class RestPullDataSetCreator extends AbstarctDataSetCreater{

    @Override
    public Dataset<Row> getDataSet(Map<String, Object> properties) throws DapServerException {
        DapConnection dapConnection=getDapConnection(properties);
        //To do until script support not come
        return null;
    }
}
