package com.dap.command.executor.dap.controller;

import com.dap.command.executor.dap.MachineArgs;
import com.dap.command.executor.dap.entity.MetaTaskInfo;
import com.dap.command.executor.dap.exceptions.DapServerException;
import com.dap.command.executor.dap.repository.MetaTaskInfoRepository;
import com.dap.command.executor.dap.taskresolver.impl.executor.service.TaskExecutor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TaskListner implements ApplicationListener {

    ClientSetUp client=new ClientSetUp();


    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    MetaTaskInfoRepository metaTaskInfoRepository;

    @Transactional
    public void execute (String r){
        System.out.println("AAAAAAAAAAAAAAAAA"+r);

        Map<String, Object> map = new HashMap<String, Object>();
        Gson gson = new Gson();

        JsonObject json= gson.fromJson(r,JsonObject.class);
        MetaTaskInfo metaTaskInfo= metaTaskInfoRepository.findById(Integer.parseInt(String.valueOf(json.get("metaTaskInfo")))).get();
        System.out.println(metaTaskInfo.toString());

        for(String key:json.keySet()){
            map.put(key,json.get(key));
        }

        try {
            System.out.println("Executing Task Executor::::::::::::::::::");
            taskExecutor.execute(metaTaskInfo,map);
        } catch (DapServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        String consumerClientID = RandomStringUtils.randomAlphabetic(10);
        System.out.println(consumerClientID);
        System.out.println(MachineArgs.serverTopicName+"@@@@@@@@@@@@@"+MachineArgs.clientTopicName);
        client.init(MachineArgs.serverTopicName,MachineArgs.clientTopicName,consumerClientID,this);
        client.SendMessage("start");
        FutureTask listner= new FutureTask<>(client.listen());
        new Thread(listner).start();

        try {
            System.out.println("@@@@@@@@@@@@@@"+listner.get().toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
