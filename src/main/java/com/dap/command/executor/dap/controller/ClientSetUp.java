package com.dap.command.executor.dap.controller;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;

public class ClientSetUp {



    private KafkaProducer kafkaProducer;
    private KafkaConsumer kafkaConsumer;
    private TaskListner taskListner;

    private String consumerTopic;
    private String producerTopic;


    private void initConsumer(String clientId) {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("group.id", "dap-group-"+clientId);
        kafkaConsumer=new KafkaConsumer(properties);
    }

    private void initProducer() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProducer=new KafkaProducer(properties);
    }

    public void init(String consumerTopic, String producerTopic, String clientId, TaskListner taskListner){
        this.consumerTopic=consumerTopic;
        this.producerTopic=producerTopic;
        this.taskListner=taskListner;
        initConsumer(clientId);
        initProducer();
        List topics = new ArrayList();
        topics.add(consumerTopic);
        kafkaConsumer.subscribe(topics);
        //    listen();
    }


    private class Listner implements Callable<String> {

        @Override
        public String call() throws Exception {
            String response=null;
            try{
                while (true){
                   // System.out.println("@");
                    ConsumerRecords records = kafkaConsumer.poll(10);
                    records.iterator().forEachRemaining(c->{});
                    for (Object record: records){
                        ConsumerRecord r= (ConsumerRecord) record;
                        response=r.value().toString();
                        System.out.println(String.format("Topic - %s, Offset - %d, Value: %s", r.topic(), r.offset(),
                                r.value()));
                        taskListner.execute(r.value().toString());

                    }
                   // if(response!=null)
                        //return response;
                }
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
            return null;
        }
    }

    public Callable<String> listen() {
        return new Listner();
    }


    public void SendMessage(String message){

        try{

            kafkaProducer.send(new ProducerRecord(producerTopic, message ));

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void close(){
        kafkaProducer.close();
        kafkaConsumer.close();
    }

}
