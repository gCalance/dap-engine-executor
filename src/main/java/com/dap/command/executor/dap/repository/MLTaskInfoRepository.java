package com.dap.command.executor.dap.repository;

import com.dap.command.executor.dap.entity.MLTaskInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MLTaskInfoRepository extends JpaRepository<MLTaskInfo, Long> {

	List<MLTaskInfo> findByModelId(String modelId);

	MLTaskInfo findByModelIdAndModelName(String modelId, String modelName);

}
