package com.dap.command.executor.dap.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class MetaFieldInfo {

	@Id
	@GeneratedValue
	private Integer id;

	private String name;
	private String displayName;
	private String type;
	private String defaultValue;
	private String hint;
	private boolean required;
	private boolean engineProperty = false;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "meta_task_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private MetaTaskInfo metaTaskInfo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName
	 *            the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public MetaTaskInfo getMetaTaskInfo() {
		return metaTaskInfo;
	}

	public void setMetaTaskInfo(MetaTaskInfo metaTaskInfo) {
		this.metaTaskInfo = metaTaskInfo;
	}

	public boolean isEngineProperty() {
		return engineProperty;
	}

	public void setEngineProperty(boolean engineProperty) {
		this.engineProperty = engineProperty;
	}

	@Override
	public String toString() {
		return "MetaFieldInfo{" + "id=" + id + ", name='" + name + '\'' + ", type='" + type + '\'' + ", defaultValue='"
				+ defaultValue + '\'' + ", hint='" + hint + '\'' + ", required=" + required + ", engineProperty="
				+ engineProperty + '}';
	}
}
